Ansible Role User Management for Linux Systems
===============================================

A role for generate users and groups on Debian based distributions using [Ansible](http://www.ansible.com)
This Ansible role is part of [Issac Guru Project: Ansible Automation Script for Linux Environments](https://gitlab.com/issac-guru/ansible/linux/).


Features
--------

- Create user
- Create groups

Requirements
------------

- Ansible 2.10.8 or higher
- Debian based distribution (may work with other versions, but has never been tested)


Role Variables
--------------

| VARIABLE                  | DESCRIPTION                                                                      |
|---------------------------|----------------------------------------------------------------------------------|
| `default_shell`           | Shell e.g. "/bin/bash"                                                           |
| `create_home_directory`   | Define if home directory should be created (valid values: true | false)          | 
| `include_user_in_sudoers` | Define if user is part of sudo group (valid values: true | false)                | 
|  `generate_ssh_keys`      | Generate SSH Keys for user (valid values: true | false)                          | 
|  `default_key_type`       | Define key type to be used e.g. "ed25519"                                        |  
|  `default_key_bits`       | Define key size e.g. 4096 - for ed25519 key type this variable is ignored        | 
|  `default_key_passphrase` | Define a passphrase to be used during user generation                            |
|  `default_key_comment`    | Define comment for key generated - for ed25519 key type this variable is ignored |
|  `user_groups`            | List of group users to be generated                                              |
|  `users`                  | List of users to be generated                                                    |


Dependencies
------------

None


Example Playbook
----------------

The following example explains how-to consume ansible role:

```yml
  - name: "Host Management Example"
    hosts: all
    gather_facts: no
    ignore_errors: True

    roles:
    - {
        role: users,
        user_group_list:
        {
          "ssh_users": { group_id: "5500", group_name: "ssh_users" },
          "dev":       { group_id: "7800", group_name: "dev" },
          "devops":    { group_name: "devops" },
          "opssys":    { group_name: "opssys", is_system_group: true },
        },
        user_list:
        {
          "ohasii": { user_id: "9001", user_name: "ohasii", home_dir: "/home/12345", shell: "/bin/bash", group: "dev", groups:" "devops" },
          "issac": { user_id: "9002", user_name: "issac", shell: "/bin/bash2", groups: "devops" },
          "teste": { user_name: "teste" },
          "test_ssh": { user_id: "1005", user_name: "ohasii", create_homedir="false", generate_keys="true", key_passphrase="alp01", key_comment="Ansible UPI" },
        }
      }
```


License
-------

  This ansible automation script is free software; you can redistribute
  it and / or modify it under the terms of the GNU General Public License
  as published by   the Free Software Foundation; either version 2 of the
  License, or any later version.

  This program is distributed in the hope that it will be usefull, but
  WITHOUT ANY WARRANTY; without even the impled warranty of MERCHANTABILITY
  or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License
  for more details.


Author Information
------------------

CREATED BY *Issac Nolis Ohasi*\
*LinkedIn Profile:* https://linkedin.com/in/ohasi \
*Gitlab Profile:* https://gitlab.com/ohasi/ \
*Website:* https://issac.guru \
*Contact:* me AT issac.guru 
